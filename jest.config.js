module.exports = {
  collectCoverageFrom: ["src/components/**/*.{ts,tsx}", "!**/node_modules/**"],
  coverageReporters: ["text"],
  testRegex: "src/.*(\\.(test|spec))\\.(tsx?)$",
  testEnvironment: "jsdom",
  moduleNameMapper: {
    "src/(.*)": "<rootDir>/src/$1",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/src/tests/__mocks__/file-mock.ts",
    "\\.(css|less|sass|scss)$": "<rootDir>/src/tests/__mocks__/style-mock.ts",
  },
};
