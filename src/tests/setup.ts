/* eslint-disable import/no-extraneous-dependencies */
import { configure } from "@testing-library/react";

configure({
  asyncUtilTimeout: 2000,
});

console.warn = jest.fn(console.warn);
console.error = jest.fn(console.error);
