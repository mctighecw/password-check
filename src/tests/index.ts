/* eslint-disable import/no-extraneous-dependencies */
import "@testing-library/jest-dom";

import "./render";
import "./setup";

export * from "@testing-library/react";
