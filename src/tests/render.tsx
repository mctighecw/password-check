import React from "react";
import { MemoryRouter, Route } from "react-router-dom";

const render = (params: string[], path: string = "/", children: React.ReactNode) => (
  <MemoryRouter initialEntries={params}>
    <Route path={path}>{children}</Route>
  </MemoryRouter>
);

export default render;
