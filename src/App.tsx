import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import Main from "src/components/main";
import "src/styles/global.css";

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Main} />
      <Route render={() => <Redirect to="/" />} />
    </Switch>
  </Router>
);

export default App;
