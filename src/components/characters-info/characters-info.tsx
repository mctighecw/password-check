import React from "react";
import { checkCharacters } from "src/utils/functions";
import { variationMessage } from "src/utils/constants";
import "./styles.less";

type CharactersInfoProps = {
  password: string;
};

const CharactersInfo = ({ password }: CharactersInfoProps) => {
  const numberUppercase = checkCharacters(password, "[A-Z]");
  const numberLowercase = checkCharacters(password, "[a-z]");
  const numberNumbers = checkCharacters(password, "[0-9]");
  const numberSpecialChars = checkCharacters(password, "[^A-Za-z0-9]");

  const displayText = (value: number) => (value > 0 ? `Yes (${value})` : "No");
  const getStyle = (value: number) => (value > 0 ? "green" : "red");
  const showVariationMessage = !numberUppercase || !numberLowercase || !numberNumbers || !numberSpecialChars;

  return (
    <div className="characters-info">
      <div className="characters-heading">Password Characters</div>

      <div className="characters-content">
        <div className="box">
          <div className="title">Uppercase letters</div>
          <div className={`value ${getStyle(numberUppercase)}`} data-testid="uppercase-characters">
            {displayText(numberUppercase)}
          </div>
        </div>

        <div className="box">
          <div className="title">Lowercase letters</div>
          <div className={`value ${getStyle(numberLowercase)}`} data-testid="lowercase-characters">
            {displayText(numberLowercase)}
          </div>
        </div>

        <div className="box last">
          <div className="title">Numbers</div>
          <div className={`value ${getStyle(numberNumbers)}`} data-testid="numbers-characters">
            {displayText(numberNumbers)}
          </div>
        </div>

        <div className="box last">
          <div className="title">Special characters</div>
          <div className={`value ${getStyle(numberSpecialChars)}`} data-testid="special-characters">
            {displayText(numberSpecialChars)}
          </div>
        </div>
      </div>

      {showVariationMessage && <div className="advice-field">{variationMessage}</div>}
    </div>
  );
};

export default CharactersInfo;
