import React from "react";
import { render, screen } from "src/tests";
import CharactersInfo from "./characters-info";

describe("CharactersInfo component", () => {
  describe("with only lowercase characters", () => {
    it("renders no uppercase characters info correctly", () => {
      const password = "abcdefg";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("uppercase-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("red")).toBe(true);
    });

    it("renders lowercase characters info correctly", () => {
      const password = "abcdefg";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("lowercase-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(7\)/i)).toBeInTheDocument();
    });

    it("renders no numbers info correctly", () => {
      const password = "abcdefg";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("numbers-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("red")).toBe(true);
    });
  });

  describe("with uppercase characters and numbers", () => {
    it("renders uppercase characters info correctly", () => {
      const password = "ABC1234";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("uppercase-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(3\)/i)).toBeInTheDocument();
    });

    it("renders numbers info correctly", () => {
      const password = "ABC1234";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("numbers-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(4\)/i)).toBeInTheDocument();
    });

    it("renders no special info correctly", () => {
      const password = "ABC1234";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("special-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("red")).toBe(true);
    });
  });

  describe("with all types of characters", () => {
    it("renders uppercase characters info correctly", () => {
      const password = "ABdef1234#";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("uppercase-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(2\)/i)).toBeInTheDocument();
    });

    it("renders lowercase characters info correctly", () => {
      const password = "ABdef1234#";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("lowercase-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(3\)/i)).toBeInTheDocument();
    });

    it("renders numbers info correctly", () => {
      const password = "ABdef1234#";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("numbers-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(4\)/i)).toBeInTheDocument();
    });

    it("renders special characters info correctly", () => {
      const password = "ABdef1234#";
      render(<CharactersInfo password={password} />);

      const field = screen.getByTestId("special-characters") as HTMLInputElement;

      expect(field).toBeInTheDocument();
      expect(field.classList.contains("value")).toBe(true);
      expect(field.classList.contains("green")).toBe(true);
      expect(screen.getByText(/yes \(1\)/i)).toBeInTheDocument();
    });
  });
});
