import React, { useState } from "react";
import PasswordInfo from "src/components/password-info";
import Footer from "src/components/footer";
import "./styles.less";

const Main = () => {
  const [password, setPassword] = useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value.replace(" ", "");
    setPassword(value);
  };

  return (
    <div className="main-container">
      <div className="content">
        <div className="heading">Test Your Password</div>

        <input
          type="text"
          value={password}
          autoFocus
          placeholder="type here"
          maxLength={50}
          onChange={handleChange}
        />

        <PasswordInfo password={password} minLength={3} />
      </div>
      <Footer />
    </div>
  );
};

export default Main;
