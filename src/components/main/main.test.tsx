import React from "react";
import { render, screen } from "src/tests";
import Main from "./main";

describe("Main component", () => {
  it("renders without crashing", () => {
    render(<Main />);
  });

  it("renders the main heading", () => {
    render(<Main />);

    const heading = screen.getByText(/test your password/i);

    expect(heading).toBeInTheDocument();
  });

  it("renders the input field", () => {
    render(<Main />);

    const textbox = screen.getByRole("textbox");

    expect(textbox).toBeInTheDocument();
  });

  it("renders the input field with focus by default", () => {
    render(<Main />);

    const textbox = screen.getByRole("textbox");

    expect(textbox).toHaveFocus();
  });

  it("renders the input field placeholder correctly", () => {
    render(<Main />);

    const textbox = screen.getByRole("textbox") as HTMLInputElement;

    expect(textbox.placeholder).toEqual("type here");
  });

  it("renders the input field maxLength correctly", () => {
    render(<Main />);

    const textbox = screen.getByRole("textbox") as HTMLInputElement;

    expect(textbox.maxLength).toEqual(50);
  });

  it("displays no value in input field", () => {
    render(<Main />);

    const textbox = screen.getByRole("textbox") as HTMLInputElement;

    expect(textbox.value).toEqual("");
  });
});
