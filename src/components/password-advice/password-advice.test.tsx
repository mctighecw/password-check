import React from "react";
import { render, screen } from "src/tests";
import PasswordAdvice from "./password-advice";

describe("PasswordAdvice component", () => {
  const renderShort = () => {
    const feedback = {
      warning: "Sequences like abc or 6543 are easy to guess",
      suggestions: ["Add another word or two. Uncommon words are better.", "Avoid sequences"],
    };
    render(<PasswordAdvice passwordLength={3} feedback={feedback} />);
  };

  it("renders short password length warning", () => {
    renderShort();

    const message = screen.getByText(/passwords 6 characters or less are very insecure!/i);

    expect(message).toBeInTheDocument();
  });

  it("renders short password advice", () => {
    renderShort();

    const message = screen.getByText(/longer passwords are much more secure than shorter ones\./i);

    expect(message).toBeInTheDocument();
  });

  it("renders password sequence warning", () => {
    renderShort();

    const message = screen.getByText(/sequences like abc or 6543 are easy to guess\./i);

    expect(message).toBeInTheDocument();
  });

  it("renders password sequence suggestion 1", () => {
    renderShort();

    const message = screen.getByText(/add another word or two\. uncommon words are better\./i);

    expect(message).toBeInTheDocument();
  });

  it("renders password sequence suggestion 2", () => {
    renderShort();

    const message = screen.getByText(/avoid sequences/i);

    expect(message).toBeInTheDocument();
  });
});
