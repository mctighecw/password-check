import React from "react";
import { getItemFromObjectByKey } from "src/utils/functions";
import { passwordLengthMessages } from "src/utils/constants";
import "./styles.less";

type FeedbackType = {
  warning: string;
  suggestions: string[];
};

type PasswordAdviceProps = {
  passwordLength: number;
  feedback: FeedbackType;
};

const PasswordAdvice = ({ passwordLength, feedback }: PasswordAdviceProps) => {
  const getPasswordLengthMessage = (type: string, minLength: number) => {
    if (passwordLength <= minLength) {
      return getItemFromObjectByKey(passwordLengthMessages, type);
    }
    return null;
  };

  if (passwordLength > 12 && !feedback?.warning && !feedback?.suggestions.length) {
    return null;
  }

  return (
    <div className="password-advice">
      <div className="advice-heading">Advice</div>

      <div className="advice-field">{getPasswordLengthMessage("veryShort", 6)}</div>
      <div className="advice-field">{getPasswordLengthMessage("lengthMessage", 12)}</div>
      {feedback.warning && <div className="advice-field">{`${feedback.warning}.`}</div>}

      {feedback.suggestions.length > 0 &&
        feedback.suggestions.map((suggestion: string) => (
          <div key={suggestion} className="advice-field">
            {suggestion}
          </div>
        ))}
    </div>
  );
};

export default PasswordAdvice;
