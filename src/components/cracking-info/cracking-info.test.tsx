import React from "react";
import { render, screen } from "src/tests";
import CrackingInfo from "./cracking-info";

describe("CrackingInfo component", () => {
  const renderShort = () => {
    const crackTimes = {
      online_throttling_100_per_hour: "5 minutes",
      online_no_throttling_10_per_second: "less than a second",
      offline_slow_hashing_1e4_per_second: "less than a second",
      offline_fast_hashing_1e10_per_second: "less than a second",
    };
    render(<CrackingInfo crackTimes={crackTimes} />);
  };

  const renderLong = () => {
    const crackTimes = {
      online_throttling_100_per_hour: "centuries",
      online_no_throttling_10_per_second: "3 years",
      offline_slow_hashing_1e4_per_second: "1 day",
      offline_fast_hashing_1e10_per_second: "less than a second",
    };
    render(<CrackingInfo crackTimes={crackTimes} />);
  };

  it("renders the cracking info heading", () => {
    renderShort();

    const heading = screen.getByText(/cracking times/i);

    expect(heading).toBeInTheDocument();
  });

  it("renders the cracking info sub-headings", () => {
    renderShort();

    const subHeading1 = screen.getByText(/offline fast hashing \(10 bill. per sec\.\)/i);
    const subHeading2 = screen.getByText(/offline slow hashing \(10k per sec\.\)/i);
    const subHeading3 = screen.getByText(/online no throttling \(10 per sec\.\)/i);
    const subHeading4 = screen.getByText(/online throttling \(100 per hr\.\)/i);

    expect(subHeading1).toBeInTheDocument();
    expect(subHeading2).toBeInTheDocument();
    expect(subHeading3).toBeInTheDocument();
    expect(subHeading4).toBeInTheDocument();
  });

  it("renders 'Offline fast hashing' value correctly", () => {
    renderLong();

    const field = screen.getByTestId("cracking-value1");

    expect(field).toBeInTheDocument();
    expect(field.textContent).toBe("less than a second");
  });

  it("renders 'Offline slow hashing' value correctly", () => {
    renderLong();

    const field = screen.getByTestId("cracking-value2");

    expect(field).toBeInTheDocument();
    expect(field.textContent).toBe("1 day");
  });

  it("renders 'Online no throttling' value correctly", () => {
    renderLong();

    const field = screen.getByTestId("cracking-value3");

    expect(field).toBeInTheDocument();
    expect(field.textContent).toBe("3 years");
  });

  it("renders 'Online throttling' value correctly", () => {
    renderLong();

    const field = screen.getByTestId("cracking-value4");

    expect(field).toBeInTheDocument();
    expect(field.textContent).toBe("centuries");
  });
});
