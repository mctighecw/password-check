import React from "react";
import { camelizeData } from "src/utils/functions";
import "./styles.less";

type CrackingInfoProps = {
  crackTimes: {
    online_throttling_100_per_hour: string;
    online_no_throttling_10_per_second: string;
    offline_slow_hashing_1e4_per_second: string;
    offline_fast_hashing_1e10_per_second: string;
  };
};

const CrackingInfo = ({ crackTimes }: CrackingInfoProps) => {
  const {
    onlineThrottling100PerHour,
    onlineNoThrottling10PerSecond,
    offlineSlowHashing1e4PerSecond,
    offlineFastHashing1e10PerSecond,
  } = camelizeData(crackTimes);

  return (
    <div className="cracking-info">
      <div className="cracking-heading">Cracking Times</div>

      <div className="cracking-content">
        <div className="box">
          <div className="title">Offline fast hashing (10 bill. per sec.)</div>
          <div className="value" data-testid="cracking-value1">
            {offlineFastHashing1e10PerSecond}
          </div>
        </div>

        <div className="box">
          <div className="title">Offline slow hashing (10k per sec.)</div>
          <div className="value" data-testid="cracking-value2">
            {offlineSlowHashing1e4PerSecond}
          </div>
        </div>

        <div className="box">
          <div className="title">Online no throttling (10 per sec.)</div>
          <div className="value" data-testid="cracking-value3">
            {onlineNoThrottling10PerSecond}
          </div>
        </div>

        <div className="box last">
          <div className="title">Online throttling (100 per hr.)</div>
          <div className="value" data-testid="cracking-value4">
            {onlineThrottling100PerHour}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CrackingInfo;
