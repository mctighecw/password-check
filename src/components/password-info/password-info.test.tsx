import React from "react";
import { render, screen } from "src/tests";
import PasswordInfo from "./password-info";

describe("PasswordInfo component", () => {
  it("renders without crashing", () => {
    render(<PasswordInfo password="password" minLength={3} />);
  });

  it("renders correct message when no password entered", () => {
    render(<PasswordInfo password="" minLength={3} />);

    const message = screen.getByText(
      /enter a password to test its strength, get some advice for improving it, and see some estimated cracking times\./i
    );

    expect(message).toBeInTheDocument();
  });

  it("renders correct message when two characters entered", () => {
    render(<PasswordInfo password="ab" minLength={3} />);

    const message = screen.getByText(/keep typing\.\.\./i);

    expect(message).toBeInTheDocument();
  });

  it("renders correct strength rating when very weak password entered", () => {
    render(<PasswordInfo password="password" minLength={3} />);

    const strengthRating = screen.getByText(/very weak/i);

    expect(strengthRating).toBeInTheDocument();
  });

  it("renders correct guesses number when very weak password entered", () => {
    render(<PasswordInfo password="password" minLength={3} />);

    const guesses = screen.getByText(/guesses required: 3/i);

    expect(guesses).toBeInTheDocument();
  });

  it("renders correct strength rating when moderate password entered", () => {
    render(<PasswordInfo password="pAsw0rD!" minLength={3} />);

    const strengthRating = screen.getByText(/moderate/i);

    expect(strengthRating).toBeInTheDocument();
  });

  it("renders correct guesses number when moderate password entered", () => {
    render(<PasswordInfo password="pAsw0rD!" minLength={3} />);

    const guesses = screen.getByText(/guesses required: 3\.3\+ million/i);

    expect(guesses).toBeInTheDocument();
  });

  it("renders correct strength rating when very strong password entered", () => {
    render(<PasswordInfo password="sF#yU3*MV5G@" minLength={3} />);

    const strengthRating = screen.getByText(/very strong/i);

    expect(strengthRating).toBeInTheDocument();
  });

  it("renders correct guesses number when very strong password entered", () => {
    render(<PasswordInfo password="sF#yU3*MV5G@" minLength={3} />);

    const guesses = screen.getByText(/guesses required: 1\+ trillion/i);

    expect(guesses).toBeInTheDocument();
  });
});
