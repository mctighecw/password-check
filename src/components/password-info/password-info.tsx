import React from "react";
import zxcvbn from "zxcvbn";
import { colors, labels } from "src/utils/constants";
import { getItemFromObjectByIndex, formatLongNumber } from "src/utils/functions";
import StrengthBar from "src/components/strength-bar";
import PasswordAdvice from "src/components/password-advice";
import CharactersInfo from "src/components/characters-info";
import CrackingInfo from "src/components/cracking-info";
import "./styles.less";

type PasswordStrengthProps = {
  password: string;
  minLength: number;
};

const PasswordStrength = ({ password, minLength }: PasswordStrengthProps) => {
  const result = zxcvbn(password);
  const { score, feedback, guesses } = result;

  if (password.length === 0) {
    return (
      <div className="password-strength-container">
        <div className="general-directions">
          Enter a password to test its strength, get some advice for improving it, and see some estimated
          cracking times.
        </div>
      </div>
    );
  }

  if (password.length > 0 && password.length < minLength) {
    return (
      <div className="password-strength-container">
        <div className="general-directions">Keep typing...</div>
      </div>
    );
  }

  return (
    <div className="password-strength-container">
      <StrengthBar strength={score} />

      <div className="password-rating">
        <div className="password-strength-heading">Password strength:</div>
        <div
          className="password-strength-badge"
          style={{
            backgroundColor: getItemFromObjectByIndex(colors, score),
          }}
        >
          {getItemFromObjectByIndex(labels, score)}
        </div>
      </div>

      <div className="message-line">{`Guesses required: ${formatLongNumber(guesses)}`}</div>

      <PasswordAdvice passwordLength={password.length} feedback={feedback} />
      <CharactersInfo password={password} />
      <CrackingInfo crackTimes={result.crack_times_display} />
    </div>
  );
};

export default PasswordStrength;
