import React from "react";
import { render, screen } from "src/tests";

import { colors } from "src/utils/constants";
import { getItemFromObjectByIndex } from "src/utils/functions";
import StrengthBar from "./strength-bar";

describe("StrengthBar component", () => {
  it("renders without crashing", () => {
    render(<StrengthBar strength={0} />);
  });

  it("renders bar with strength 0 correctly", () => {
    const strength = 0;
    render(<StrengthBar strength={strength} />);

    const bar = screen.getByTestId("bar0");
    const color = getItemFromObjectByIndex(colors, strength);

    expect(bar).toBeInTheDocument();
    expect(bar).toHaveStyle(`background-color: ${color}`);
  });

  it("renders bar with strength 1 correctly", () => {
    const strength = 1;
    render(<StrengthBar strength={strength} />);

    const bar = screen.getByTestId("bar1");
    const color = getItemFromObjectByIndex(colors, strength);

    expect(bar).toBeInTheDocument();
    expect(bar).toHaveStyle(`background-color: ${color}`);
  });

  it("renders bar with strength 2 correctly", () => {
    const strength = 2;
    render(<StrengthBar strength={strength} />);

    const bar = screen.getByTestId("bar2");
    const color = getItemFromObjectByIndex(colors, strength);

    expect(bar).toBeInTheDocument();
    expect(bar).toHaveStyle(`background-color: ${color}`);
  });

  it("renders bar with strength 3 correctly", () => {
    const strength = 3;
    render(<StrengthBar strength={strength} />);

    const bar = screen.getByTestId("bar3");
    const color = getItemFromObjectByIndex(colors, strength);

    expect(bar).toBeInTheDocument();
    expect(bar).toHaveStyle(`background-color: ${color}`);
  });

  it("renders bar with strength 4 correctly", () => {
    const strength = 4;
    render(<StrengthBar strength={strength} />);

    const bar = screen.getByTestId("bar4");
    const color = getItemFromObjectByIndex(colors, strength);

    expect(bar).toBeInTheDocument();
    expect(bar).toHaveStyle(`background-color: ${color}`);
  });
});
