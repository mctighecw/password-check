import React from "react";
import { colors } from "src/utils/constants";
import { getItemFromObjectByIndex } from "src/utils/functions";
import "./styles.less";

type StrengthBarProps = {
  strength: number;
};

const StrengthBar = ({ strength }: StrengthBarProps) => {
  const color = getItemFromObjectByIndex(colors, strength);

  const getStyle = (index: number) => {
    if (strength >= index) {
      return { backgroundColor: color };
    }
    return {};
  };

  return (
    <div className="strength-bar">
      <div className="bar" style={{ backgroundColor: color }} data-testid="bar0" />
      <div className="bar" style={getStyle(1)} data-testid="bar1" />
      <div className="bar" style={getStyle(2)} data-testid="bar2" />
      <div className="bar" style={getStyle(3)} data-testid="bar3" />
      <div className="bar" style={getStyle(4)} data-testid="bar4" />
    </div>
  );
};

export default StrengthBar;
