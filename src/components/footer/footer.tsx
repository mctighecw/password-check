import React from "react";
import "./styles.less";

const Footer = () => (
  <div className="footer-container">
    <div className="footer">&#169; 2021 Christian McTighe. Coded by Hand.</div>
  </div>
);

export default Footer;
