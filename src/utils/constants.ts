export const colors = {
  0: "#ec1d25",
  1: "#ec403c",
  2: "#f58225",
  3: "#70bf42",
  4: "#01a55e",
};

export const labels = {
  0: "Very weak",
  1: "Weak",
  2: "Moderate",
  3: "Strong",
  4: "Very strong",
};

export const passwordLengthMessages = {
  veryShort: "Passwords 6 characters or less are very insecure!",
  lengthMessage: "Longer passwords are much more secure than shorter ones.",
};

export const variationMessage = "The more types of characters used, the more secure your password will be.";
