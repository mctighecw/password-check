export const snakeToCamel = (value: string) =>
  value.toLowerCase().replace(/[-_][a-z0-9]/g, (group) => group.slice(-1).toUpperCase());

export const camelizeData = (object: any) => {
  const newObject: { [key: string]: string } = object;
  const result: { [key: string]: string } = {};

  Object.keys(newObject).forEach((key: string) => {
    const newKey = snakeToCamel(key);
    result[newKey] = newObject[key];
  });
  return result;
};

export const getItemFromObjectByIndex = (object: any, index: number) => {
  const newObject: { [key: number]: string } = object;
  return newObject[index];
};

export const getItemFromObjectByKey = (object: any, key: string) => {
  const newObject: { [key: string]: string } = object;
  return newObject[key];
};

export const checkCharacters = (password: string, expression: string) => {
  if (password) {
    const regex = new RegExp(expression, "g");
    return password.match(regex)?.length || 0;
  }
  return 0;
};

export const formatLongNumber = (num: number, digits: number = 2) => {
  if (num && num >= 1_000_000) {
    const si = [
      { value: 1e6, label: "million" },
      { value: 1e9, label: "billion" },
      { value: 1e12, label: "trillion" },
      { value: 1e15, label: "quadrillion" },
      { value: 1e18, label: "quintillion" },
      { value: 1e21, label: "sextillion" },
      { value: 1e24, label: "septillion" },
      { value: 1e27, label: "octillion" },
      { value: 1e30, label: "nonillion" },
      { value: 1e33, label: "decillion" },
    ];

    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    let i;

    for (i = si.length - 1; i > 0; i -= 1) {
      if (num >= si[i].value) {
        break;
      }
    }

    const base = (num / si[i].value).toFixed(digits).replace(rx, "$1");
    const { label } = si[i];
    return `${base}+ ${label}`;
  }
  return num.toLocaleString("en-US");
};
