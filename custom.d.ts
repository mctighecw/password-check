declare function require(name: string);

declare module "react-dom";
declare module "react-markup";
declare module "zxcvbn";

declare module "*.css";
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.png";

declare module "*.svg" {
  const content: any;
  export default content;
}
