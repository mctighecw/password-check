# README

A small React app to provide information about password strength.

It uses _TypeScript_, _ESLint_, and _Prettier_, for very clean, nicely formatted, and correctly typed code. (For the best editing/debugging results, use the _VS Code_ editor.)

The initial commit can also serve as a project template.

## App Information

App Name: password-check

Created: November-December 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/password-check)

Production: [Link](https://password-check.mctighecw.site)

## Tech Stack

- React
- TypeScript
- Less & CSS
- Webpack
- ESLint
- Prettier
- Jest & React Testing Library
- Docker

## To Run

_Development_

```sh
$ yarn install
$ yarn start
```

_Production_

Using `docker run`

```sh
$ docker build \
  -f Dockerfile.prod \
  -t password-check . \
  && docker run \
  --rm -it \
  --name password-check \
  -p 80:80 \
  password-check
```

## To Test

Tests with _Jest_ and _React Testing Library_.

```sh
$ yarn test
```

Last updated: 2024-12-17
