const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { merge } = require("webpack-merge");

const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "production",
  output: {
    filename: "[contenthash:10].js",
    chunkFilename: "[contenthash:10].js",
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.less$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "less-loader"],
      },
      {
        test: /\.(gif|ico|jpe?g|pdf|png|svg)$/,
        use: "file-loader?name=assets/[contenthash:10].[ext]",
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
      },
    }),
    new MiniCssExtractPlugin({
      filename: "[contenthash:10].css",
      chunkFilename: "[contenthash:10].css",
    }),
  ],
  devtool: false,
  optimization: {
    namedModules: true,
    namedChunks: true,
    minimizer: [
      new OptimizeCssAssetsPlugin(),
      new TerserPlugin({
        extractComments: true,
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          ecma: 6,
          warnings: false,
          mangle: true,
          module: false,
          toplevel: false,
          ie8: false,
          keep_fnames: false,
          safari10: false,
          extractComments: "all",
          compress: {
            drop_console: true,
          },
        },
      }),
    ],
  },
});
